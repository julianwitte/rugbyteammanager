﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamPositionView : MonoBehaviour {

    public Athlete currentAthlete;
    public string position;
    /*
    winger
    full-back
    lock
    centre
    out-half
    scrum-half
    prop
    hooker
    flanker
    number-eight
    */

    public GameObject tooltipGroup;
    public Text textName;
    public Image imageFrame;
    public AthleteView athleteView;
    Color defaultColor = Color.white;

    public void SetColor(Color color)
    {
        defaultColor = color;
        textName.color = color;
        DefaultColor();
    }

	public void UpdateAvailability(Athlete athlete)
    {
        if (athlete.Positions.Contains(position))
        {
            if(currentAthlete != null)
            {
                imageFrame.color = Color.yellow;
            }
            else
            {
                imageFrame.color = Color.cyan;
            }
        }
        else
        {
            imageFrame.color = Color.grey;
        }
    }

    public void DefaultColor()
    {
        imageFrame.color = defaultColor;
    }

    public void SetAthlete(Athlete athlete)
    {
        currentAthlete = athlete;
        athleteView.SetModel(athlete);
        
    }

    public void RemoveAthlete()
    {
        currentAthlete = null;
        athleteView.RemoveModel();
    }

    public void Hover(bool hover)
    {
        if(currentAthlete != null)
            tooltipGroup.SetActive(hover);
        else
            tooltipGroup.SetActive(false);
    }

}
