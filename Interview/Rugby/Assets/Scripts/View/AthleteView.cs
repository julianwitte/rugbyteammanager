﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AthleteView : MonoBehaviour {

    public Text textName;
    public Text textPositions;
    public Text textStarRating;
    public Image imageAvatar;
    public Text textInjured;
    public Text textCountry;
    public Text textLastPlayed;

	public void SetModel(Athlete model)
    {
        if (textName != null)
            textName.text = model.Name;

        if (textPositions != null)
        {
            textPositions.text = "";
            for (int i = 0; i < model.Positions.Count; i++)
            {
                var position = model.Positions[i] + ((i+1 < model.Positions.Count) ? ", " : "");
                textPositions.text += position;
            }
        }

        if(textStarRating != null)
        {
            textStarRating.text = "";
            for (int i = 0; i < model.StarRating; i++)
            {
                textStarRating.text += "*";
            }
        }

        if(textInjured != null)
        {
            textInjured.gameObject.SetActive(model.IsInjured);
        }

        if(textCountry != null)
        {
            textCountry.text = model.Country;
        }

        if(textLastPlayed != null)
        {
            textLastPlayed.text = model.LastPlayed;
        }

        AthleteRequester.GetInstance().RequestAthletePicture(model, SetImage);
    }

    public void RemoveModel()
    {
        if (textName != null)
            textName.text = "";

        if (textPositions != null)
        {
            textPositions.text = "";
        }

        if (textStarRating != null)
        {
            textStarRating.text = "";
        }

        if (textInjured != null)
        {
            textInjured.gameObject.SetActive(false);
        }

        if (textCountry != null)
        {
            textCountry.text = "";
        }

        if (textLastPlayed != null)
        {
            textLastPlayed.text = "";
        }

        SetImage(null);
    }

    void SetImage(Texture2D texture)
    {
        if (texture == null)
            imageAvatar.overrideSprite = null;
        else
            imageAvatar.overrideSprite = Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
    }

    public void IsValid(bool valid)
    {
        if(imageAvatar != null)
        {
            imageAvatar.color = valid ? Color.white : Color.gray;
        }
    }
}
