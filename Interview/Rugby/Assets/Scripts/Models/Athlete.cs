﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

public class Athlete {

    [JsonProperty("name")]
    public string Name { get; set; }
    [JsonProperty("avatar_url")]
    public string AvatarURL { get; set; }
    [JsonProperty("star_rating")]
    public int StarRating { get; set; }
    [JsonProperty("country")]
    public string Country { get; set; }
    [JsonProperty("last_played")]
    public string LastPlayed { get; set; }
    [JsonProperty("positions")]
    public List<string> Positions { get; set; }
    [JsonProperty("is_injured")]
    public bool IsInjured { get; set; }
    [JsonProperty("id")]
    public long ID { get; set; }

    public Athlete () { }

}

public class JsonAthletes
{
    public Athlete[] athletes;
}
