﻿using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Newtonsoft.Json;

public class AthleteRequester : Singleton<AthleteRequester> {

    string athletes_url = "https://raw.githubusercontent.com/KitmanLabs/interview_screeners/master/unity/athletes.json";

    public void RequestAthletes(Action<Athlete[]> callback)
    {
        StartCoroutine(GetAthletesFromURL(callback));
    }

    IEnumerator GetAthletesFromURL(Action<Athlete[]> callback) // Using a coroutine just to avoid stalling the app while downloading data from url.
    {
        WWW www = new WWW(athletes_url);
        yield return www;
        try
        {
            var json = www.text;
            var jsonAthletes = JsonConvert.DeserializeObject<JsonAthletes>(json);
            callback(jsonAthletes.athletes);
        }
        catch (Exception e)
        {
            Debug.LogWarning("Failed to download athletes from URL. " + e.Message);
            callback(null);
        }
         yield return null;
    }

    public void RequestAthletePicture(Athlete athlete, Action<Texture2D> callback)
    {
        StartCoroutine(GetAthletePictureFromURL(athlete, callback));
    }

    IEnumerator GetAthletePictureFromURL(Athlete athlete, Action<Texture2D> callback)
    {
        var www = new WWW(athlete.AvatarURL);
        yield return www;
        try
        {
            var avatar = www.texture;
            callback(avatar);
        }
        catch (Exception e)
        {
            Debug.LogWarningFormat("Failed to download Athlete {0} avatar from URL. " + e.Message, athlete.Name);
        }
    }



}
