﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Newtonsoft.Json;

public class TeamManager : MonoBehaviour {

    // Athletes
    public string localAthletesData;
    Athlete[] athletesPool;
    List<long> equippedAthletes = new List<long>();
    int selectedAthlete;

    // Team management
    public TeamPositionView[] team1PositionViews; // represent each position in the field.
    public TeamPositionView[] team2PositionViews;

    public Color team1Color = Color.blue;
    public Color team2Color = Color.red;

    public Text team1Name;
    public Text team2Name;


    public Text team1StarRating;
    public Text team2StarRating;

    // UI
    public Canvas uiCanvas;
    public ScrollRect scrollView; // for the athletes pool.
    public AthleteView athleteScrollWidgetPrefab; // used to represent each athlete in the scroll view.
    public DraggableImageView draggableImageView; // to represent the athlete currentry being dragged.
    public AthleteView athleteInfoPanel;
    bool draggingAthlete = false;

    public delegate void AthleteHandler(long id);
    public event AthleteHandler EquippedAthlete;
    public event AthleteHandler RemovedAthlete;



    void Start()
    {
        AthleteRequester.GetInstance().RequestAthletes(LoadedAthletes);

        team2PositionViews = new TeamPositionView[team1PositionViews.Length];
        for (int i = 0; i < team1PositionViews.Length; i++)
        {
            team2PositionViews[i] = Instantiate(team1PositionViews[i], team1PositionViews[i].transform.parent);
            team2PositionViews[i].transform.localPosition *= -1f;
            team1PositionViews[i].SetColor(team1Color);
            team2PositionViews[i].SetColor(team2Color);
            team1PositionViews[i].name = "Team 1 - " + team1PositionViews[i].position;
            team2PositionViews[i].name = "Team 2 - " + team1PositionViews[i].position;

            SetupTeamPositionTrigger(team1PositionViews[i]);
            SetupTeamPositionTrigger(team2PositionViews[i]);
        }

        UpdateTeamStarRating();
        EquippedAthlete += (id) => UpdateTeamStarRating();
        RemovedAthlete += (id) => UpdateTeamStarRating();

        team1Name.color = team1Color;
        team2Name.color = team2Color;
    }

    void SetupTeamPositionTrigger(TeamPositionView teamPositionView)
    {
        var trigger = teamPositionView.GetComponent<EventTrigger>();
        var entry = new EventTrigger.Entry();
        float lastClick = 0f;
        float doubleClickThreshold = 0.3f;
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) =>
        {
            Debug.Log("Clicked Position");
            if(Time.realtimeSinceStartup - lastClick < doubleClickThreshold)
            {
                var athlete = teamPositionView.currentAthlete;
                if (athlete != null)
                {
                    teamPositionView.RemoveAthlete();
                    RemoveAthlete(athlete.ID);
                }
            }
            lastClick = Time.realtimeSinceStartup;
        });
        trigger.triggers.Add(entry);
    }

    void RemoveAthlete(long id)
    {
        equippedAthletes.Remove(id);
        if(RemovedAthlete != null)
        {
            RemovedAthlete(id);
        }
    }

    void UpdateTeamStarRating()
    {
        int team1 = 0;
        for (int i = 0; i < team1PositionViews.Length; i++)
        {
            if(team1PositionViews[i].currentAthlete != null)
            {
                team1 += team1PositionViews[i].currentAthlete.StarRating;
            }
        }
        team1 = team1 / team1PositionViews.Length;

        int team2 = 0;
        for (int i = 0; i < team2PositionViews.Length; i++)
        {
            if (team2PositionViews[i].currentAthlete != null)
            {
                team2 += team2PositionViews[i].currentAthlete.StarRating;
            }
        }
        team2 = team2 / team2PositionViews.Length;

        string team1Text = "";
        for (int i = 0; i < team1; i++)
        {
            team1Text += "*";
        }
        string team2Text = "";
        for (int i = 0; i < team2; i++)
        {
            team2Text += "*";
        }
        team1StarRating.text = team1Text;
        team2StarRating.text = team2Text;
    }

    string ColorToHex(Color32 color)
    {
        string hex = "[" + color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2") + "]";
        return hex;
    }

    void LoadedAthletes(Athlete[] athletes)
    {
        if(athletes == null || athletes.Length == 0)
        {
            Debug.LogWarning("Athlete List is null or Empty. Using local data.");
            athletesPool = JsonConvert.DeserializeObject<JsonAthletes>(localAthletesData).athletes;
        }
        else
        {
            athletesPool = athletes;
        }
        UpdateAthletesPoolView();
    }

    void UpdateAthletesPoolView()
    {
        var size = athletesPool.Length * 110f;
        scrollView.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size);
        for (int i = 0; i < athletesPool.Length; i++)
        {
            var instance = Instantiate(athleteScrollWidgetPrefab, scrollView.content);
            var rect = instance.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector3(0f, (size * 0.5f) -50f - (i * 110f), 0f);
            instance.SetModel(athletesPool[i]);
            var trigger = instance.imageAvatar.GetComponent<EventTrigger>();
            int index = i;

            EventTrigger.Entry beginDragEntry = new EventTrigger.Entry();
            beginDragEntry.eventID = EventTriggerType.BeginDrag;
            beginDragEntry.callback.AddListener((data) => BeginDragAthleteFromPool(index));
            EventTrigger.Entry draggingEntry = new EventTrigger.Entry();
            draggingEntry.eventID = EventTriggerType.Drag;
            draggingEntry.callback.AddListener((data) => OnDraggingAthlete((PointerEventData)data));
            EventTrigger.Entry endDragEntry = new EventTrigger.Entry();
            endDragEntry.eventID = EventTriggerType.EndDrag;
            endDragEntry.callback.AddListener((data) => EndDragAthlete((PointerEventData)data));

            EventTrigger.Entry clickEntry = new EventTrigger.Entry();
            clickEntry.eventID = EventTriggerType.PointerClick;
            clickEntry.callback.AddListener((data) => ShowAthleteInfo(athletesPool[index]));

            trigger.triggers.Add(beginDragEntry);
            trigger.triggers.Add(draggingEntry);
            trigger.triggers.Add(endDragEntry);
            trigger.triggers.Add(clickEntry);

            EquippedAthlete += (id) =>
            {
                if(id.Equals(athletesPool[index].ID))
                {
                    instance.IsValid(false);
                }
            };
            RemovedAthlete += (id) =>
            {
                if (id.Equals(athletesPool[index].ID))
                {
                    instance.IsValid(true);
                }
            };
        }
    }

    #region Drag and Drop

    void BeginDragAthleteFromPool(int index)
    {
        if (!athletesPool[index].IsInjured && !equippedAthletes.Contains(athletesPool[index].ID))
        {
            Debug.Log("Begin Drag " + index);
            selectedAthlete = index;
            scrollView.StopMovement();
            draggableImageView.gameObject.SetActive(true);
            AthleteRequester.GetInstance().RequestAthletePicture(athletesPool[index], (texture) => draggableImageView.SetImage(texture));

            for (int i = 0; i < team1PositionViews.Length; i++)
            {
                team1PositionViews[i].UpdateAvailability(athletesPool[selectedAthlete]);
                team2PositionViews[i].UpdateAvailability(athletesPool[selectedAthlete]);
            }
            draggingAthlete = true;
        }
    }

    void EndDragAthlete(PointerEventData data)
    {
        if (!draggingAthlete)
            return;
        Debug.Log("End Drag " + selectedAthlete);
        draggableImageView.gameObject.SetActive(false);
        for (int i = 0; i < team1PositionViews.Length; i++)
        {
            team1PositionViews[i].DefaultColor();
            team2PositionViews[i].DefaultColor();
        }
        draggingAthlete = false;
        var raycaster = uiCanvas.GetComponent<GraphicRaycaster>();
        var results = new List<RaycastResult>();
        raycaster.Raycast(data, results);
        for (int i = 0; i < results.Count; i++)
        {
            var positionView = results[i].gameObject.GetComponent<TeamPositionView>();
            if(positionView != null)
            {
                if(athletesPool[selectedAthlete].Positions.Contains(positionView.position))
                {
                    if (positionView.currentAthlete != null)
                        RemoveAthlete(positionView.currentAthlete.ID);
                    positionView.SetAthlete(athletesPool[selectedAthlete]);
                    equippedAthletes.Add(athletesPool[selectedAthlete].ID);
                    if (EquippedAthlete != null)
                        EquippedAthlete(athletesPool[selectedAthlete].ID);
                }
                else
                {
                    Debug.LogWarning("Invalid Position");
                }
            }
        }
    }


    void OnDraggingAthlete(PointerEventData data)
    {
        if (!draggingAthlete)
            return;
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(uiCanvas.transform as RectTransform, data.position, uiCanvas.worldCamera, out pos);
        draggableImageView.transform.position = uiCanvas.transform.TransformPoint(pos);
    }

    #endregion

    void ShowAthleteInfo(Athlete athlete)
    {
        athleteInfoPanel.SetModel(athlete);
        athleteInfoPanel.gameObject.SetActive(true);
    }

}
