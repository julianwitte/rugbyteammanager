﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

    static T _instance;
    public static T GetInstance()
    {
        if(_instance == null)
        {
            var go = new GameObject(typeof(T).Name + "(Singleton)");
            _instance = go.AddComponent<T>();
            DontDestroyOnLoad(go);
        }
        return _instance;
    }
}
